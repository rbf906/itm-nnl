Sprache: Wordbildung

Paradigmenbeschreibung:
Abwechselnde Sprach- (Wortbildung anhand eines visuell pr�sentierten Buchstabens) und Ruhebl�cke, je 30 Sekunden lang;
1 Ruheblock plus 4 Zyklen (B  AB  AB  AB  AB);
Buchstaben: 6 (E, R, N, A, S, T), 3 pro Sprachblock; Buchstabenabfolge randomisiert innerhalb des Blocks;
---
Instruktionen:
Instruieren Sie den Patienten entsprechend �ber die Aufgabe und f�hren Sie ein Training durch, um sicherzustellen, dass der Patient seine Aufgabe versteht. F�r diese Aufgabe sieht der Patient unterscheidliche Buchstaben f�r je einige Sekunden. Aufgabe des Patienten ist es so viele W�rter wie m�glich zu bilden, die mit dem gezeigten Buchstaben beginnen.
W�hrend des darauf folgenden Ruheblocks sollte der Patient ruhig liegen und sich auf das Fixationskreuz in der Mitte das Bildschirms konzentrieren. Die Sprach- und Ruhebl�cke wechseln mehrmals ab.
---
Einstellungen:
TR: 3000 ms
Messungen: 90
