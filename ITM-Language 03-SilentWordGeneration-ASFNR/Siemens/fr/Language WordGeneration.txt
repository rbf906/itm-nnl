G�n�ration linguistique de mots

Description du paradigme:
Les p�riodes  alternent  de g�n�ration de mots et de pause, de 30 secondes chacune;
1 pause plus 4 cycles (B  AB  AB  AB  AB);
nombre total de lettres initiales: 6 (E, R, N, A, S, T), 3 par p�riode; r�partition al�atoire dans les p�riodes;

Instructions:
Donnez aux patients  les instructions appropri�es et informez-les suffisamment �  fin d��tre s�r qu'ils ont compris les t�ches � ex�cuter!
Les patients voient s'afficher une lettre � la fois, qui reste plusieurs secondes � l'�cran. Ils doivent alors penser � tous les mots possibles commen�ant par la lettre donn�e. Trois lettres diff�rentes vont s'afficher au cours de chaque p�riode de g�n�ration de mots. Chaque p�riode de g�n�ration de mots est suivie d'une p�riode de pause. Au cours de la p�riode de pause, les patients voient une mire au milieu de l'�cran. Ils doivent rester immobiles et se concentrer sur cette mire.
Les p�riodes de g�n�ration de mots et de pause se succ�dent plusieurs fois

Param�tres:
Temps TR: 3000 ms
Nombre de volumes: 90
