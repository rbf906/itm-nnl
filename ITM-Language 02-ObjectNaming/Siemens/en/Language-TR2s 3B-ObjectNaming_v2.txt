
Language: Object Naming

Paradigm description: 
Alternating picture naming and rest blocks, 20 seconds each;
1 OFF + 6 cycles (B AB AB AB AB AB AB);
Total number of initial pictures: 30, 5 per block; within-block randomization;
---
Instructions:
Please instruct patients appropriately and conduct patient training to ensure patients understand their responsibilities!
Patients will be presented with one picture at a time, which will remain on the screen for several seconds. The patients’ job is to think of the name of the picture. Five different pictures will be presented during each picture naming block. Every picture naming block will be followed by a rest period. During the rest period patients will see a fixation cross in the center of the screen. Patients should remain still and rest, focusing on the fixation.
The picture naming and rest periods will alternate several times.
---
Always remember to start the paradigm before you start the MR scanner! 
Press NEXT to continue the study.
	