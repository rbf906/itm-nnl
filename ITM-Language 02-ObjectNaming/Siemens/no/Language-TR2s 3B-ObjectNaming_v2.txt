
Språkoppgave: Navngi objekter

Beskrivelse av paradigmet:
Vekslende perioder med bilder og hvile, hver på 20 sekunder; 
1 OFF + 6 sykluser (B AB AB AB AB AB AB);
Antall bilder: 30, 5 per blokk; randomisering innad i blokken;

---
Instruksjoner: 
Vennligst gi pasienten grundige instrukser og opplæring, for å forsikre deg om at de forstår hva de skal gjøre!
I oppgaveperioden vil pasienten se bilder av ulike objekter, som vil vises på skjermen i flere sekunder. Oppgaven er å tenke på navnet til det som blir vist. Fem ulike bilder blir vist per blokk. Hver oppgaveperiode etterfølges av en hvileperiode. Da skal pasienten ligge rolig, hvile og fokusere på krysset som vises midt på skjermen. Periodene med bilder og hvile vil bli gjentatt flere ganger.  
---
Husk å alltid starte paradigmet før du starter MR-skanneren.
Trykk NEXT for å fortsette studien.

	