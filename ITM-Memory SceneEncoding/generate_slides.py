import random

# Set template path and output path for completed file
slidefiletemplatepath = 'ITM-Memory_SceneEncoding_slides_Template.xml'
slidefileoutpath = 'ITM-Memory_SceneEncoding_slides.xml'

# Generate number lists to be shuffled

mem_slides_per_block = 4
num_slides_per_block = 8
blocks = 4
slides = mem_slides_per_block*blocks

imagesize = 256
arrowsize = 320

objects = list(range(slides))
scenes = list(range(slides))

# Shuffle the objects, scenes
random.shuffle(objects)
random.shuffle(scenes)

print("OBJECTS:\n",objects)
print("SCENES:\n",scenes)

# Generate PairSlides
pairslides = ''
for b in range(blocks):
    for s in range(mem_slides_per_block):
        i = b * mem_slides_per_block + s
        thisslide = ('\t<Slide>\n'
                    +'\t\t<name>Pair'+str((b+1)*100+s+1)+'</name>\n'
                    +'\t\t<Picture>./objects/'+str(objects[i]+1).zfill(3)+'.jpg</Picture>\n'
                    +'\t\t<position>LtImagePos</position>\n'
                    +'\t\t<width>'+str(imagesize)+'</width>\n'
                    +'\t\t<Picture>./scenes/'+str(scenes[i]+1).zfill(3)+'.jpg</Picture>\n'
                    +'\t\t<position>RtImagePos</position>\n'
                    +'\t\t<width>'+str(imagesize)+'</width>\n'
                    +'\t\t<Picture>./graphics/lt_arrow_F.jpg</Picture>\n'
                    +'\t\t<position>LtArrowPos</position>\n'
                    +'\t\t<width>'+str(arrowsize)+'</width>\n'
                    +'\t\t<Picture>./graphics/rt_arrow_NF.jpg</Picture>\n'
                    +'\t\t<position>RtArrowPos</position>\n'
                    +'\t\t<width>'+str(arrowsize)+'</width>\n'
                    +'\t\t<expectedResponse>\n'
                        +'\t\t\t<key>AB</key>\n'
                        +'\t\t\t<key>A</key>\n'
                        +'\t\t\t<key>B</key>\n'
                        +'\t\t\t<key>CD</key>\n'
                        +'\t\t\t<key>C</key>\n'
                        +'\t\t\t<key>D</key>\n'
                        +'\t\t\t<count>1</count>\n'
                    +'\t\t</expectedResponse>\n'
                    +'\t</Slide>\n')
        pairslides = pairslides+thisslide


# Read the file contents
with open(slidefiletemplatepath,'r') as slidefile:
    slidetext = slidefile.read()

# Replace the relevant codes with the randomised file names
for i in range(slides):
    #~ slidetext = slidetext.replace('{OBJ'+str(i+1).zfill(3)+'}',str(objects[i]+1).zfill(3)+'.jpg')
    #~ slidetext = slidetext.replace('{SCE'+str(i+1).zfill(3)+'}',str(scenes[i]+1).zfill(3)+'.jpg')
    slidetext = slidetext.replace('{PAIRSLIDES}',pairslides)

# Generate code for number slides
numslides = ''
thisnum=0
lastnum=0
for b in range(blocks*2):
    for s in range(num_slides_per_block):
        while thisnum==lastnum:
            # Put it in while loop to avoid immediate repeat numbers
            thisnum = random.randint(1,99)
        if thisnum%2==0:
            response='\t\t\t<key>AB</key>\n\t\t\t<key>A</key>\n\t\t\t<key>B</key>\n'
        else:
            response='\t\t\t<key>CD</key>\n\t\t\t<key>C</key>\n\t\t\t<key>D</key>\n'
        if b%2==0:
            sub = 'A'
        else:
            sub = 'B'
        slide = ('\t<Slide>\n'
                +'\t\t<name>Num'+str((b//2+1))+sub+str(s+1).zfill(2)+'</name>\n'
                +'\t\t<text>'+str(thisnum)+'</text>\n'
                +'\t\t<position>NumberPos</position>\n'
                +'\t\t<Picture>./graphics/lt_arrow_even.jpg</Picture>\n'
                +'\t\t<position>LtArrowPos</position>\n'
                +'\t\t<width>'+str(arrowsize)+'</width>\n'
                +'\t\t<Picture>./graphics/rt_arrow_odd.jpg</Picture>\n'
                +'\t\t<position>RtArrowPos</position>\n'
                +'\t\t<width>'+str(arrowsize)+'</width>\n'
                +'\t\t<expectedResponse>\n'
                +response
                +'\t\t</expectedResponse>\n'
                +'\t\t<count>1</count>\n'
                +'\t</Slide>\n'
                )
        numslides = numslides+(slide)
        lastnum = thisnum

slidetext = slidetext.replace('{NUMSLIDES}',numslides)

# Generate code for recall slides
# using shuffled recall list from earlier

recallslides = ''
for b in range(blocks):
    recall_short = list(range(mem_slides_per_block))
    random.shuffle(recall_short)
    for s in range(mem_slides_per_block):
        # Use random number to select object or scene
        thisnum = random.randint(1,100)
        thisslide = b*mem_slides_per_block+recall_short[s]
        if thisnum%2==0:
            impath='./objects/'+str(objects[thisslide]+1).zfill(3)+'.jpg'       
        else:
            impath='./scenes/'+str(scenes[thisslide]+1).zfill(3)+'.jpg'
        response = '\t\t\t<key>AB</key>\n\t\t\t<key>A</key>\n\t\t\t<key>B</key>\n'
        
        slide = ('\t<Slide>\n'
                +'\t\t<name>Recall'+str((b+1)*100+s+1)+'</name>\n'
                +'\t\t<Picture>'+impath+'</Picture>\n'
                +'\t\t<position>MidImagePos</position>\n'
                +'\t\t<width>'+str(imagesize)+'</width>\n'
                +'\t\t<Picture>./graphics/lt_arrow_yes.jpg</Picture>\n'
                +'\t\t<position>LtArrowPos</position>\n'
                +'\t\t<width>'+str(arrowsize)+'</width>\n'
                +'\t\t<Picture>./graphics/rt_arrow_no.jpg</Picture>\n'
                +'\t\t<position>RtArrowPos</position>\n'
                +'\t\t<width>'+str(arrowsize)+'</width>\n'
                +'\t\t<expectedResponse>\n'
                +response
                +'\t\t</expectedResponse>\n'
                +'\t\t<count>1</count>\n'
                +'\t</Slide>\n'
                )
        recallslides = recallslides+slide

slidetext = slidetext.replace('{RECALLSLIDES}',recallslides)


# Write the contents to the slides file
with open(slidefileoutpath,'w') as outfile:
    outfile.write(slidetext)


# Generate Trials file
trialfiletemplatepath = 'ITM-Memory_SceneEncoding_trials_Template.xml'
trialfileoutpath = 'ITM-Memory_SceneEncoding_trials.xml'

# Read the file contents
with open(trialfiletemplatepath,'r') as trialfile:
    trialtext = trialfile.read()

pairtrials = ''
for b in range(blocks):
    for s in range(mem_slides_per_block):
        trial = ('\t<Trial>\n'
                +'\t\t<name>PairTrial'+str((b+1)*100+s+1)+'</name>\n'
                +'\t\t<show>\n'
                +'\t\t\t<item>Pair'+str((b+1)*100+s+1)+'</item>\n'
                +'\t\t\t<duration>$ObjectShowTime</duration>\n'
                +'\t\t</show>\n'
                +'\t</Trial>\n'
                )
        pairtrials = pairtrials+trial

numtrials = ''
for b in range(blocks):
    for a in range(2):
        for s in range(num_slides_per_block):
            if a%2==0:
                sub='A'
            else:
                sub='B'
            trial = ('\t<Trial>\n'
                    +'\t\t<name>NumTrial'+str((b+1))+sub+str(s+1).zfill(2)+'</name>\n'
                    +'\t\t<show>\n'
                    +'\t\t\t<item>Num'+str((b+1))+sub+str(s+1).zfill(2)+'</item>\n'
                    +'\t\t\t<duration>$NumberTime</duration>\n'
                    +'\t\t</show>\n'
                    +'\t</Trial>\n'
                    )
            numtrials = numtrials+trial

recalltrials = ''
for b in range(blocks):
    for s in range(mem_slides_per_block):
        trial = ('\t<Trial>\n'
                +'\t\t<name>RecallTrial'+str((b+1)*100+s+1)+'</name>\n'
                +'\t\t<show>\n'
                +'\t\t\t<item>Recall'+str((b+1)*100+s+1)+'</item>\n'
                +'\t\t\t<duration>$ObjectShowTime</duration>\n'
                +'\t\t</show>\n'
                +'\t</Trial>\n'
                )
        recalltrials = recalltrials+trial
    
# Replace tags with trial info
trialtext = trialtext.replace('{PAIRTRIALS}',pairtrials)
trialtext = trialtext.replace('{NUMTRIALS}',numtrials)
trialtext = trialtext.replace('{RECALLTRIALS}',recalltrials)

# Write to file
with open(trialfileoutpath,'w') as outfile:
    outfile.write(trialtext)
    
# COMPLETE BLOCKS IN MAIN PARADIGM FILE

partemplatepath = 'ITM-Memory SceneEncoding_Template.xml'
paroutpath = 'ITM-Memory SceneEncoding.xml'

encodeblocks = ''
numberblocks = ''
recallblocks = ''
session = ''
for b in range(blocks):
    thisencode = ('\t<Block>\n'
                +'\t\t<name>Encode'+str(b+1)+'</name>\n')
    thisrecall = ('\t<Block>\n'
                +'\t\t<name>Recall'+str(b+1)+'</name>\n')
    for s in range(mem_slides_per_block):
        thisencslide = ('\t\t<trials>PairTrial'+str((b+1)*100+s+1)+'</trials>\n'
                    +'\t\t<trials>Blank</trials>\n')
        thisencode = thisencode+thisencslide
        thisrecslide = ('\t\t<trials>RecallTrial'+str((b+1)*100+s+1)+'</trials>\n'
                    +'\t\t<trials>Blank</trials>\n')
        thisrecall = thisrecall+thisrecslide
    thisencode = thisencode+('\t\t<condition>1</condition>\n'
                    +'\t\t<order>0</order><!--Fixed order-->\n'
                    +'\t</Block>\n')
    thisrecall = thisrecall+('\t\t<condition>1</condition>\n'
                    +'\t\t<order>0</order><!--Fixed order-->\n'
                    +'\t</Block>\n')
    encodeblocks=encodeblocks+thisencode
    recallblocks=recallblocks+thisrecall

    for a in range(2):
        if a%2==0:
            sub='A'
        else:
            sub='B'
        thisnum = ('\t<Block>\n'
                    +'\t\t<name>Numbers'+str(b+1)+sub+'</name>\n')
        for n in range(num_slides_per_block):
            thisnumslide = ('\t\t<trials>NumTrial'+str(b+1)+sub+str(n+1).zfill(2)+'</trials>\n'
                    +'\t\t<trials>BlankNumber</trials>\n')
            thisnum=thisnum+thisnumslide
        thisnum=thisnum+('\t\t<condition>1</condition>\n'
                    +'\t\t<order>0</order><!--Fixed order-->\n'
                    +'\t</Block>\n')
        numberblocks=numberblocks+thisnum

# Generate session
session = ('\t<Session>\n'
        +'\t\t<name>ITM Memory - Scene Encode</name>\n'
        +'\t\t<runtrial>Start</runtrial>\n')
for b in range(blocks):
    thisblock = ('\t\t<runblock>Encode'+str(b+1)+'</runblock>\n'
                +'\t\t<runblock>Numbers'+str(b+1)+'A'+'</runblock>\n'
                +'\t\t<runblock>Recall'+str(b+1)+'</runblock>\n'
                +'\t\t<runblock>Numbers'+str(b+1)+'B'+'</runblock>\n')
    session = session+thisblock
session = session+'\t</Session>\n'

# Read the file contents
with open(partemplatepath,'r') as parfile:
    partext = parfile.read()

partext = partext.replace('{PAIRBLOCKS}',encodeblocks)
partext = partext.replace('{NUMBLOCKS}',numberblocks)
partext = partext.replace('{RECALLBLOCKS}',recallblocks)
partext = partext.replace('{SESSION}',session)

# Write to file
with open(paroutpath,'w') as outfile:
    outfile.write(partext)
    